import datetime


def now():
    return datetime.datetime.utcnow()


def from_javascript_timestamp(utc_milliseconds):
    millisecond = 1e3
    utc = utc_milliseconds / millisecond
    return datetime.datetime.utcfromtimestamp(utc)


def to_javascript_timestamp(python_datetime):
    utc = python_datetime.replace(tzinfo=datetime.timezone.utc).timestamp()
    millisecond = 1e3
    return utc * millisecond
