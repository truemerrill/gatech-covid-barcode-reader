from flask_sqlalchemy import SQLAlchemy
from .jsend import JSendBaseQuery
from .timestamp import to_javascript_timestamp


db = SQLAlchemy(query_class=JSendBaseQuery)

REGISTRATION_TOKEN_LENGTH = 8
TEST_KIT_BARCODE_LENGTH = 8


# --------------------------------------------------------------------------- #
# Data Models                                                                 #
# --------------------------------------------------------------------------- #


class Registration(db.Model):
    """Registration and survey data from Qualtrics

    Attributes:
        id (int): Registration id
        first_name (string): Participant first name
        last_name (string): Participant last name
        phone (string): Participant contact phone number
        email (string): Participant contact email
        gtid (string): Participant gtID number
        timestamp (DateTime): Time the registration was completed
        token (string): Registration token used to pair the test kit
        response_id (string): Response ID from the Qualtrics survey
        test_kit (string): Barcode of the paired test kit
        survey (JSON): Arbitrary JSON data representing the survey response
    """
    id = db.Column(db.Integer, primary_key=True)
    first_name = db.Column(db.String(64), nullable=False)
    last_name = db.Column(db.String(64), nullable=False)
    phone = db.Column(db.String(16), nullable=False)
    email = db.Column(db.String(64), nullable=False)
    gtid = db.Column(db.String(16), nullable=False)
    token = db.Column(db.String(REGISTRATION_TOKEN_LENGTH), nullable=False)
    response_id = db.Column(db.String(64), nullable=False)

    timestamp = db.Column(db.DateTime)
    test_kit = db.Column(db.String(TEST_KIT_BARCODE_LENGTH))
    survey = db.Column(db.JSON)


