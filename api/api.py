import json

from flask import (Blueprint, request, abort)
from jsonschema import (validate, ValidationError, SchemaError)
from random import randrange

import api.jsend as jsend
from .models import (db, Registration, REGISTRATION_TOKEN_LENGTH)
from .timestamp import (now, from_javascript_timestamp)


main = Blueprint('main', __name__)
api_v1 = Blueprint('api_v1', __name__, url_prefix='/api/v1')


def get_default(obj, key, default=None):
    """Get a key from obj, returning a default value if the key is not defined.

    Args:
        obj (dict) Dict like object
        key (obj) Key
        default (obj, optional) Default value, defaults to None

    Returns:
        value (obj)
    """
    try:
        res = obj[key]
    except KeyError:
        res = default
    return res


def get_timestamp(obj, key, default=None):
    """Get a key from an obj, interpreting it as a JavaScript timestamp if it
    is defined.

    Args:
        obj (dict): Dict like object
        key (obj): Key
        default (obj, optional): Default value, defaults to None

    Returns:
        datetime.datetime
    """
    try:
        res = from_javascript_timestamp(obj[key])
    except KeyError:
        res = default
    return res


def get_unique_registration_token(length=REGISTRATION_TOKEN_LENGTH):
    token = ''.join([str(randrange(10)) for _ in range(length)])
    prior_registration = (Registration.query
                          .filter(Registration.token == token)
                          .first())
    if prior_registration:
        return get_unique_registration_token(length)
    return token


@api_v1.route('/register', methods=('POST',))
def create_registration():

    _schema = {
        "type": "object",
        "properties": {
            "firstName": {
                "type": "string",
                "maxLength": 64
            },
            "lastName": {
                "type": "string",
                "maxLength": 64
            },
            "phone": {
                "type": "string",
                "maxLength": 16,
                "minLength": 9
            },
            "email": {
                "type": "string",
                "maxLength": 64
            },
            "gtID": {
                "type": "string",
                "maxLength": 16
            },
            "testKit": {
                "type": "string",
                "maxLength": 8,
                "minLength": 8
            },
            "survey": {
                "type": "object"
            }
        },
        "required": [
            "firstName",
            "lastName",
            "phone",
            "email",
            "gtID"
        ]
    }

    qualtrics_response_id = request.args.get('responseId')
    print('RESPONSE ID: {}'.format(qualtrics_response_id))

    input = request.get_json()
    try:
        validate(input, schema=_schema)
    except (ValidationError, SchemaError) as e:
        print('ERROR: {}'.format(str(e)))
        return jsend.fail(str(e))

    print('INPUT: {}'.format(input))

    r = Registration(
        first_name=input['firstName'],
        last_name=input['lastName'],
        phone=input['phone'],
        email=input['email'],
        gtid=input['gtID'],
        survey=get_default(input, 'survey'),
        response_id=qualtrics_response_id,
        token=get_unique_registration_token(),
        test_kit=get_default(input, 'testKit'),
        timestamp=now()
    )

    print('REGISTRATION: {}'.format(r))

    db.session.add(r)
    db.session.commit()

    return jsend.success({
        'id': r.id,
        'token': r.token
    })


@main.route('/register')
def get_register():
    qualtrics_response_id = request.args.get('responseId')
    r = (Registration.query
         .filter(Registration.response_id == qualtrics_response_id)
         .first())
    if not r:
        abort(404)

    return "Welcome {} {}!".format(r.first_name, r.last_name)


# # -----------------------------------------------------------------------------
# # Scans are temporary in-memory storage and are wiped whenever the application
# # is restarted.
# #
# # Create:   POST    /api/v1/scan
# # Read:     GET     /api/v1/scan
# # Delete:   DELETE  /api/v1/scan
# # -----------------------------------------------------------------------------


# class BarcodeStore(object):
#     """In memory storage of a single barcode.  Avoids use of globals.
#     """

#     def __init__(self):
#         self._barcode = None

#     def get_barcode(self):
#         return self._barcode

#     def set_barcode(self, barcode):
#         self._barcode = barcode


# # In memory, wiped on restart.
# _barcode_store = BarcodeStore()


# @api_v1.route('/scan', methods=('POST',))
# def create_scan(barcode_store=_barcode_store):
#     """Scans a barcode
#     """
#     _schema = {
#         "type": "string"
#     }

#     input = request.get_json()
#     try:
#         validate(input, schema=_schema)
#     except (ValidationError, SchemaError) as e:
#         return jsend.fail(str(e))

#     barcode_store.set_barcode(input)
#     return jsend.success(input)


# @api_v1.route('/scan', methods=('GET',))
# def read_scan(barcode_store=_barcode_store):
#     """Reads a barcode
#     """
#     barcode = barcode_store.get_barcode()
#     if barcode is None:
#         return jsend.error(ValueError('No pending barcode'))
#     return jsend.success(barcode)


# @api_v1.route('/scan', methods=('DELETE',))
# def delete_scan(barcode_store=_barcode_store):
#     barcode = barcode_store.get_barcode()
#     barcode_store.set_barcode(None)
#     return jsend.success(barcode)


# # -----------------------------------------------------------------------------
# # CRUD for endpoints
# #
# # Create:   POST    /api/v1/endpoint
# #           POST    /api/v1/endpoint/register
# # Read:     GET     /api/v1/endpoint
# #           GET     /api/v1/endpoint/<int:endpoint_id>
# # Update:   PUT     /api/v1/endpoint/<int:endpoint_id>
# # Delete:   DELETE  /api/v1/endpoint/<int:endpoint_id>
# # -----------------------------------------------------------------------------

# def _make_document_schema(field, schema):
#     """Make a DocumentSchema.  The schema must not already exist in the
#     database.  This does not add the schema object to the current session.

#     Args:
#         field (str) Unique name that identifies the schema
#         schema (dict) JSON schema used to validate documents

#     Returns:
#         ds (DocumentSchema)
#     """
#     ds = Schema(
#         field=field,
#         schema=schema
#     )
#     return ds


# def _get_document_schema(field):
#     """Get a DocumentSchema.  If the schema is not in the database this
#     function returns None.

#     Args:
#         field (str) Unique name that identifies the schema

#     Returns:
#         ds (DocumentSchema, None) A DocumentSchema object if it already exists
#             in the database, otherwise None.
#     """
#     ds = (Schema.query
#           .filter(Schema.field == field)
#           .first())
#     return ds


# def _resolve_document_schema(field, schema):
#     ds = _get_document_schema(field)
#     if ds is None:
#         ds = _make_document_schema(field, schema)

#     # Check that the schema match
#     if ds.schema != schema:
#         message = ('The schema for field \'{}\' has '.format(field) +
#                    'already been declared and does not match. ' +
#                    'Either adopt the previously declared schema ' +
#                    'or use a new field name.\n' +
#                    '\n' +
#                    'Expected Schema:\n' +
#                    '--------------- \n' +
#                    '{}'.format(json.dumps(ds.schema, indent=2)))
#         raise ValueError(message)
#     return ds


# _schema_endpoint = {
#     "type": "object",
#     "properties": {
#         "key": {
#             "type": "string",
#             "minLength": 32,
#             "maxLength": 32
#         },
#         "url": {
#             "type": "string",
#             "maxLength": 256
#         },
#         "workflow": {
#             "type": "string",
#             "maxLength": 64
#         },
#         "step": {
#             "type": "string",
#             "maxLength": 64
#         },
#         "worker": {
#             "type": "string",
#             "maxLength": 64
#         },
#         "desc": {
#             "type": "string",
#             "maxLength": 4096
#         },
#         "fields": {
#             "type": "object",
#             "patternProperties": {
#                 "^.*_": {
#                     "type": "object"
#                 }
#             }
#         }
#     },
#     "required": [
#         "key",
#         "url",
#         "workflow",
#         "step",
#         "worker",
#         "desc"
#     ]
# }


# @api_v1.route('/endpoint', methods=('POST',))
# def create_endpoint():
#     input = request.get_json()
#     try:
#         validate(input, schema=_schema_endpoint)
#     except (ValidationError, SchemaError) as e:
#         return jsend.fail(str(e))

#     # Check if this key has been used already
#     endpoint = (Endpoint.query
#                 .filter(Endpoint.key == input['key'])
#                 .all())
#     if endpoint:
#         return jsend.fail('Key is already registered')

#     # Create new endpoint
#     endpoint = Endpoint(
#         url=input['url'],
#         key=input['key'],
#         workflow=input['workflow'],
#         step=input['step'],
#         worker=input['worker'],
#         desc=input['desc'],
#     )

#     # Resolve the document schema
#     for field, schema in get_default(input, 'fields', {}).items():
#         try:
#             s = _resolve_document_schema(field, schema)
#         except ValueError as e:
#             return jsend.fail(str(e))
#         endpoint.fields.append(s)

#     db.session.add(endpoint)
#     db.session.commit()

#     return jsend.success(endpoint.to_dict())


# @api_v1.route('/endpoint/register', methods=('POST',))
# def register_endpoint():
#     _schema = {
#         "type": "object",
#         "properties": {
#             "url": {
#                 "type": "string"
#             },
#             "fields": {
#                 "type": "object",
#                 "patternProperties": {
#                     "^.*_": {
#                         "type": "object"
#                     }
#                 }
#             }
#         },
#         "required": ["url"]
#     }

#     input = request.get_json()
#     try:
#         validate(input, schema=_schema)
#         register_url = input['url']
#         fields = get_default(input, 'fields', {})
#     except (ValidationError, SchemaError) as e:
#         return jsend.fail(str(e))

#     if register_url == '':
#         return jsend.error('Empty registration URL string.')

#     # Post to remote server's registration hook
#     try:
#         r = requests.post(register_url)
#         try:
#             r.raise_for_status()
#         except requests.ConnectionError:
#             return jsend.error(('Could not reach \"{}\". '.format(register_url) +
#                                 'Check the registration URL and your internet '
#                                 'connection.'), r.status_code)

#         # Check that the server sent back required data
#         json = r.json()
#         data = json['data']

#     except requests.RequestException:
#         return jsend.error('Invalid request.')
#     except Exception as e:
#         return jsend.error(e)

#     try:
#         validate(data, schema=_schema_endpoint)
#     except (ValidationError, SchemaError) as e:
#         return jsend.fail(str(e))

#     # Create the endpoint
#     e = Endpoint(
#         url=data['url'],
#         key=data['key'],
#         workflow=data['workflow'],
#         step=data['step'],
#         worker=data['worker'],
#         desc=data['desc']
#     )

#     # Resolve the document schema
#     for field, schema in fields.items():
#         try:
#             s = _resolve_document_schema(field, schema)
#         except ValueError as e:
#             return jsend.fail(str(e))
#         e.fields.append(s)

#     db.session.add(e)
#     db.session.commit()

#     return jsend.success(e.to_dict())


# @api_v1.route('/endpoint')
# def read_endpoints():
#     return jsend.success([e.to_dict() for e in Endpoint.query.all()])


# @api_v1.route('/endpoint/<int:endpoint_id>')
# def read_endpoint(endpoint_id):
#     e = Endpoint.query.get_or_jsend_fail(endpoint_id, 'No such endpoint')
#     return jsend.success(e.to_dict())


# @api_v1.route('/endpoint/<int:endpoint_id>', methods=('PUT',))
# def update_endpoint(endpoint_id):
#     e = Endpoint.query.get_or_jsend_fail(endpoint_id, 'No such endpoint')
#     input = request.get_json()
#     try:
#         validate(input, schema=_schema_endpoint)
#     except (ValidationError, SchemaError) as err:
#         return jsend.error(err)

#     e.url = input['url']
#     e.key = input['key']
#     e.workflow = input['workflow']
#     e.step = input['step']
#     e.worker = input['worker']
#     e.desc = input['desc']

#     # Remove old schema
#     e.fields = []
#     for field, schema in get_default(input, 'fields', {}).items():
#         try:
#             s = _resolve_document_schema(field, schema)
#         except ValueError as err:
#             return jsend.fail(str(err))
#         e.fields.append(s)

#     db.session.add(e)
#     db.session.commit()

#     return jsend.success(e.to_dict())


# @api_v1.route('/endpoint/<int:endpoint_id>', methods=('DELETE',))
# def delete_endpoint(endpoint_id):
#     e = Endpoint.query.get_or_jsend_fail(endpoint_id, 'No such endpoint')
#     db.session.delete(e)
#     db.session.commit()
#     return jsend.success(endpoint_id)


# # -----------------------------------------------------------------------------
# # CRUD for batches
# #
# # Create:   POST    /api/v1/batch
# # Read:     GET     /api/v1/batch
# #           GET     /api/v1/batch/<int:batch_id>
# # Update:   PUT     /api/v1/batch/<int:batch_id>
# # Delete:   DELETE  /api/v1/batch/<int:batch_id>
# # Sync:     PUT     /api/v1/batch/<int:batch_id>/sync
# # -----------------------------------------------------------------------------

# # Schema for 'by_worklist' style batches
# _schema_by_worklist = {
#     "type": "object",
#     "properties": {
#         "batchCode": {
#             "type": "string",
#             "maxLength": 64,
#         },
#         "endpointId": {
#             "type": "integer"
#         },
#         "startTime": {
#             "type": "integer"
#         },
#         "stopTime": {
#             "type": "integer"
#         },
#         "submitTime": {
#             "type": "integer"
#         },
#         "state": {
#             "type": "integer",
#             "minimum": 0,
#             "maximum": 3
#         },
#         "worklist": {
#             "type": "array",
#             "items": {
#                 "type": "string",
#                 "maxLength": 64
#             }
#         }
#     },
#     "required": [
#         "batchCode",
#         "endpointId",
#         "worklist"
#     ]
# }


# # Schema for 'by_batch' style batches
# _schema_by_batch = {
#     "type": "object",
#     "properties": {
#         "batchCode": {
#             "type": "string",
#             "maxLength": 64,
#         },
#         "numSamples": {
#             "type": "integer",
#             "minimum": 0
#         },
#         "endpointId": {
#             "type": "integer"
#         },
#         "startTime": {
#             "type": "integer"
#         },
#         "stopTime": {
#             "type": "integer"
#         },
#         "submitTime": {
#             "type": "integer"
#         },
#         "state": {
#             "type": "integer",
#             "minimum": 0,
#             "maximum": 3
#         }
#     },
#     "required": [
#         "batchCode",
#         "numSamples",
#         "endpointId"
#     ]
# }


# _schema_batch = {
#     "anyOf": [_schema_by_worklist, _schema_by_batch]
# }


# @api_v1.route('/batch', methods=('POST',))
# def create_batch():
#     input = request.get_json()
#     try:
#         validate(input, schema=_schema_batch)
#     except (ValidationError, SchemaError) as e:
#         return jsend.error(e)

#     # Check if the batch_code has been used already
#     b = (Batch.query
#          .filter(Batch.batch_code == input['batchCode'])
#          .all())
#     if b:
#         msg = 'Batch code \'{}\' is already registered'.format(
#             input['batchCode'])
#         return jsend.fail(msg)

#     # Check that endpoint exists
#     e = Endpoint.query.get_or_jsend_fail(input['endpointId'])
#     b = Batch(
#         batch_code=input['batchCode'],
#         num_samples=get_default(input, 'numSamples', 0),
#         endpoint_id=input['endpointId'],
#         start_time=get_timestamp(input, 'startTime'),
#         stop_time=get_timestamp(input, 'stopTime'),
#         sync_time=get_timestamp(input, 'syncTime'),
#         sync_id=get_default(input, 'syncId'),
#         state=get_default(input, 'state', BatchState.NOT_STARTED)
#     )

#     # If we have a worklist, add each of the samples and overwrite num_samples
#     for sample_code in get_default(input, 'worklist', []):
#         s = Sample(sample_code=sample_code)
#         b.worklist.append(s)

#     if len(b.worklist) > 0:
#         b.num_samples = len(b.worklist)

#     # Resolve fields
#     for f in e.fields:

#         try:
#             doc = input['fields'][f.field]
#             validate(doc, f.schema)
#         except (KeyError, ValidationError, SchemaError) as e:
#             return jsend.fail(str(e))

#         # Add JSON document to represent field data
#         d = Document(
#             document=doc,
#             schema_id=f.id,
#             batch_id=b.id
#         )

#         b.fields.append(d)

#     db.session.add(b)
#     db.session.commit()

#     return jsend.success(b.to_dict())


# @api_v1.route('/batch')
# def read_batches():
#     return jsend.success([b.to_dict() for b in Batch.query.all()])


# @api_v1.route('/batch/<int:batch_id>')
# def read_batch(batch_id):
#     b = Batch.query.get_or_jsend_fail(batch_id, 'No such batch')
#     return jsend.success(b.to_dict())


# @api_v1.route('/batch/<int:batch_id>', methods=('PUT',))
# def update_batch(batch_id):
#     b = Batch.query.get_or_jsend_fail(batch_id, 'No such batch')

#     input = request.get_json()
#     try:
#         validate(input, schema=_schema_batch)
#     except (ValidationError, SchemaError) as err:
#         return jsend.error(err)

#     # Check that endpoint exists
#     e = Endpoint.query.get_or_jsend_fail(input['endpointId'])

#     # Required keys
#     b.batch_code = input['batchCode']
#     b.endpoint_id = input['endpointId']

#     # Not required
#     b.num_samples = get_default(input, 'numSamples', 0)
#     b.state = get_default(input, 'state', BatchState.NOT_STARTED)
#     b.start_time = get_timestamp(input, 'startTime')
#     b.stop_time = get_timestamp(input, 'stopTime')
#     b.sync_time = get_timestamp(input, 'syncTime')
#     b.sync_id = get_default(input, 'syncId')

#     b.worklist = []
#     for sample_code in get_default(input, 'worklist', []):
#         s = Sample(sample_code=sample_code)
#         b.worklist.append(s)

#     if len(b.worklist) > 0:
#         b.num_samples = len(b.worklist)

#     # Resolve fields
#     for d in b.fields:
#         db.session.delete(d)

#     b.fields = []
#     for f in e.fields:

#         try:
#             doc = input['fields'][f.field]
#             validate(doc, f.schema)
#         except (KeyError, ValidationError, SchemaError) as e:
#             return jsend.fail(str(e))

#         # Add JSON document to represent field data
#         d = Document(
#             document=doc,
#             schema_id=f.id,
#             batch_id=b.id
#         )

#         b.fields.append(d)

#     db.session.add(b)
#     db.session.commit()
#     return jsend.success(b.to_dict())


# @api_v1.route('/batch/<int:batch_id>', methods=('DELETE',))
# def delete_batch(batch_id):
#     b = Batch.query.get_or_jsend_fail(batch_id, 'No such batch')

#     # Delete documents
#     for d in b.fields:
#         db.session.delete(d)

#     db.session.delete(b)
#     db.session.commit()
#     return jsend.success(batch_id)


# @api_v1.route('/batch/<int:batch_id>/sync', methods=('PUT',))
# def sync_batch(batch_id):
#     b = Batch.query.get_or_jsend_fail(batch_id, 'No such batch')
#     sync_url = b.endpoint.url

#     # Is this the first sync, or are we updating an older record?
#     if b.sync_id is None:
#         try:
#             r = requests.post(sync_url, json=b.to_dict())
#             r.raise_for_status()
#         except requests.RequestException as e:
#             return jsend.error(e)

#         b_new = r.json()['data']
#         b.sync_id = b_new['syncId']
#         b.sync_time = now()

#         db.session.add(b)
#         db.session.commit()

#     else:
#         try:
#             r = requests.put(sync_url, json=b.to_dict())
#             r.raise_for_status()
#         except requests.RequestException as e:
#             return jsend.error(e)

#         b.sync_time = now()
#         db.session.add(b)
#         db.session.commit()

#     return jsend.success(b.to_dict())
