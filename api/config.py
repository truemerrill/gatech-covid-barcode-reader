import os


# General config
HOST = os.environ.get('HOST', default='http://localhost')
PORT = os.environ.get('PORT', default='5000')
TESTING = os.environ.get('TESTING', default=False)
DEBUG = os.environ.get('DEBUG', default=False)
SECRET_KEY = os.environ.get('SECRET_KEY', default=None)

# Database
SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URL',
                                         default='sqlite://')
SQLALCHEMY_TRACK_MODIFICATIONS = False
