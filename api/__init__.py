import os
from flask import Flask
from flask_migrate import Migrate


migrate = Migrate()


def create_database(app, db):
    """Create the database and initialize with required data

    Args:
        app (Flask): Flask application
        db (FlaskSQLAlchemy): Database connection
    """
    db.create_all()
    db.session.commit()


def create_app(test_config=None, instance_path=None, testing=None):
    """Application factory

    Args:
        test_config: Path to (optional) configuration file
        instance_path: Path to instance directory
        testing (bool): Whether the app is being used in a unit test

    Returns: Flask application
    """
    app = Flask(__name__, instance_relative_config=True,
                instance_path=instance_path)

    # Ensure the instance folder exists
    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass

    if test_config is None:
        # Load the instance config file
        import api.config as config
        app.config.from_object(config)
        app.config.from_pyfile('config.py', silent=True)

    else:
        # Load the test config
        import api.config as config
        app.config.from_object(config)
        app.config.from_pyfile(test_config)

    if testing:
        # Allow override sent from unit test framework
        app.testing = testing

    # Imports
    from .api import api_v1, main
    from .models import db

    app.register_blueprint(api_v1)
    app.register_blueprint(main)
    db.init_app(app)
    migrate.init_app(app, db)
    
    # Create the database
    @app.before_first_request
    def _create_database():
        create_database(app, db)

    return app
