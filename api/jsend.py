import inspect
from flask import (current_app, jsonify, abort, make_response)
from flask_sqlalchemy import (BaseQuery)


# -----------------------------------------------------------------------------
# Simple implementation of the JSend standard.
#
# See https://github.com/omniti-labs/jsend
# -----------------------------------------------------------------------------


def success(data=None, code=200):
    """Returns data as a JSend-compliant message
    """
    return jsonify({
        'status': 'success',
        'data': data
    }), code


def fail(data=None, code=400):
    """Returns a failure message as a JSend-compliant message
    """
    return jsonify({
        'status': 'fail',
        'data': data
    }), code


def error(err, code=500):
    """Returns error as a JSend-compliant message
    """
    current_app.logger.error(str(err))
    return jsonify({
        'status': 'error',
        'message': str(err),
        'code': code
    }), code


def _default_callback():
    return abort(404)


class JSendBaseQuery(BaseQuery):
    """Modifies the SQLAlchemy base query to add new `get_or_*` and
    `first_or_*` helper methods that return JSend compliant errors.
    """

    def get_or_callback(self, ident, callback=_default_callback):
        """Like :meth:`get` but aborts with a callback if not found instead of
        returning ``None``.
        """
        rv = self.get(ident)
        if rv is None:
            return abort(make_response(callback()))
        return rv

    def first_or_callback(self, callback=_default_callback):
        """Like :meth:`first` but aborts with a callback if not found instead
        of returning ``None``.
        """
        rv = self.first()
        if rv is None:
            return abort(make_response(callback()))
        return rv

    def get_or_jsend_success(self, ident, data=None, code=200):
        def _callback():
            return success(data, code)
        return self.get_or_callback(ident, _callback)

    def get_or_jsend_fail(self, ident, data=None, code=400):
        def _callback():
            return fail(data, code)
        return self.get_or_callback(ident, _callback)

    def get_or_jsend_error(self, ident, data=None, code=500):
        def _callback():
            return error(ValueError(data), code)
        return self.get_or_callback(ident, _callback)

    def first_or_jsend_success(self, data=None, code=200):
        def _callback():
            return success(data, code)
        return self.first_or_callback(_callback)

    def first_or_jsend_fail(self, data=None, code=400):
        def _callback():
            return fail(data, code)
        return self.first_or_callback(_callback)

    def first_or_jsend_error(self, data=None, code=500):
        def _callback():
            return error(ValueError(data), code)
        return self.first_or_callback(_callback)
