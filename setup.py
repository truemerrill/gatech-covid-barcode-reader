#!/usr/bin/env python
import os

from setuptools import setup

setup(
    name='gatech-covid-test-registration',
    version='0.1',
    description='Application for registering GT COVID-19 tests',
    author='True Merrill',
    author_email='true.merrill@gtri.gatech.edu',
    license='MIT',
    packages=['api'],
    install_requires=[
        'flask',
        'flask-sqlalchemy',
        'flask-testing',
        'flask-migrate',
        'python-barcode',
        'jsonschema',
        'requests',
        'requests-mock',
        'psycopg2',
        'gunicorn'
    ]
)

