import React, {useState, useEffect} from 'react';
import Quagga from 'quagga';


// React app

function App() {

  // App state
  const [barcode, setBarcode] = useState('');

  const majorityVote = () => {
    const histogram = {};
    
    function addHistogram(histogram, key) {
      if (key in histogram) {
        histogram[key] += 1;
      } else {
        histogram[key] = 1;
      }
    }

    function highestFrequency() {
      const numKeys = Object.keys(histogram).length;
      if (numKeys === 0) {
        return '';
      } else if (numKeys === 1) {
        return Object.keys(histogram)[0];
      } else {
        return Object.keys(histogram).reduce((a, b) => 
          histogram[a] > histogram[b] ? a : b
        );
      }
    }
    
    return (data) => {
      const code = data.codeResult.code;
      addHistogram(histogram, code);
      setBarcode(highestFrequency(histogram));
      console.log(histogram);
    }
  };
  
  useEffect(() => {
    Quagga.init({
      inputStream : {
        name : "Live",
        type : "LiveStream",
        target: document.querySelector('#yourElement')    // Or '#yourElement' (optional)
      },
      decoder : {
        readers : ["code_128_reader"]
      }
    }, function(err) {
        if (err) {
            console.log(err);
            return
        }
        console.log("Initialization finished. Ready to start");
        Quagga.start();
    });

    // On detect callback
    Quagga.onDetected(majorityVote());
  });

  return (
    <div className="App">
        <h1>A trivial app</h1>
        <div id='yourElement'></div>
        <div>
          <p>Barcode: {barcode}</p>
        </div>
    </div>
  );
}

export default App;
